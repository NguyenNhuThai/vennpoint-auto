package com.hmvn.autotest.core;

import java.util.HashMap;
import java.util.List;

import org.testng.ITestResult;
import org.testng.Reporter;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class APIUtil {
	private static final String regisApi = "api.regisnewrun";
	private static final String getRuntimeApi = "api.getruntime";
	private static final String postResultApi = "api.postresult";
	
	private static	JsonObject testData = Json.getJsonData("testLinkID");
	
	public static void regisNewBuildTime(String suiteName, String time, String ip){
		String url = PropertiesHelper.getApi(regisApi);
		HashMap<String, String> data = new HashMap<String, String>();
		data.put("SuiteName", suiteName);
		data.put("Time", time);
		data.put("IP", ip);
		HttpUtil.postData(url, data);
	}
	
	public static String getRuntime(String suiteName) {
		String runtime = "";
		String url = PropertiesHelper.getApi(getRuntimeApi)+ suiteName;
		System.out.println("get runtime url: " + url);
		JsonArray jsonArray = (JsonArray) Json.getJsonElement(url, "GET");
		JsonObject jsonObject = (JsonObject) jsonArray.get(0);
		if (jsonObject.has("RunTime")) {
			runtime = jsonObject.get("RunTime").getAsString();
		}
		return runtime;
	}
	public static void pushPassTestToServer(ITestResult result, String Base64){
		String url = PropertiesHelper.getApi(postResultApi);
		HashMap<String, String> data = new HashMap<String, String>();
		data.put("RunTime", Constant.currentRuntime);
		data.put("TestLinkId", testData.get(result.getName()).getAsString());
		data.put("Name", result.getName());
		data.put("Message", "");
		data.put("SuiteName", result.getTestContext().getSuite().getName());
		data.put("Result", "passed");
		data.put("Exception", "");
		data.put("imageBase64", Base64);
		HttpUtil.postData(url, data);
	}
	
	public static void pushFailTestToServer(ITestResult result, String Base64){
		String url = PropertiesHelper.getApi(postResultApi);
		StackTraceElement[] listStack = result.getThrowable().getStackTrace();
		String log = "";
		for (int i = 0; i < listStack.length; i++) {
			log += listStack[i].toString() +"\n";
		}
		HashMap<String, String> data = new HashMap<String, String>();
		data.put("RunTime", Constant.currentRuntime);
		data.put("TestLinkId", testData.get(result.getName()).getAsString());
		data.put("Name", result.getName());
		data.put("Message", result.getThrowable().getMessage());
		data.put("Result", "failed");
		data.put("Exception", log);
		data.put("SuiteName", result.getTestContext().getSuite().getName());
		data.put("imageBase64", Base64);
		HttpUtil.postData(url, data);
	}
}
