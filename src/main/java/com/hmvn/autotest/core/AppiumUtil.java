package com.hmvn.autotest.core;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.URL;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class AppiumUtil {
	private static AndroidDriver driver;
	private static IOSDriver iosDriver;
	public static final String APP_DIR = "project.appdir";
	public static final String PROP_PROJECT_BASE_DIR = "project.basedir";
	public static final String PROP_APPIUM_SERVER = "appium.server";

	public static AndroidDriver getDriverInstance(String platformName, String platformVersion, String deviceName, String appName, String appPackage) {
		Constant.currentPlatform = "mobile_android";
		DriverUtil.browserRunning = "mobile_app";
		try{
			driver = null;
			File dir = new File(PropertiesHelper.getKey(PROP_PROJECT_BASE_DIR) + File.separator + PropertiesHelper.getKey(APP_DIR));
			File app = new File(dir,appName);
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability("platformName", platformName);
			cap.setCapability("platformVersion", platformVersion);
			cap.setCapability("deviceName", deviceName);
			cap.setCapability("appPackage",  appPackage);
			cap.setCapability("app", app.getAbsolutePath());

			driver = new AndroidDriver(new URL(PropertiesHelper.getKey(PROP_APPIUM_SERVER)),cap);
			DriverUtil.driverMobileApp = driver;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return driver;
	}
	public static IOSDriver getIosDriverInstance(String platformName, String platformVersion, String deviceName, String appName) {
		Constant.currentPlatform = "mobile_ios";
		DriverUtil.browserRunning = "mobile_app";
		try{
			iosDriver = null;
			File dir = new File(PropertiesHelper.getKey(PROP_PROJECT_BASE_DIR) + File.separator + PropertiesHelper.getKey(APP_DIR));
			File app = new File(dir,appName);
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("platformName", platformName);
			caps.setCapability("platformVersion", platformVersion); //Replace this with your iOS version
			caps.setCapability("deviceName", deviceName); //Replace this with your simulator/device version
			//scaps.setCapability("app", app.getAbsolutePath()); //Replace this with app path in your system
			caps.setCapability("app", app.getAbsolutePath());
			caps.setCapability("noReset", true); 
			iosDriver = new IOSDriver(new URL(PropertiesHelper.getKey(PROP_APPIUM_SERVER)),caps);
			DriverUtil.driverMobileApp = driver;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return iosDriver;
	}
	
	public static AppiumDriver getCurrentDriver() {
		if (Constant.currentPlatform.contains("ios")) {
			return iosDriver;
		} else{
			return driver;
		}

	}

	public static void startServer(String environment) {
		if(environment.contains("mac")){
			startServerMac();
		}else{
			startServerWindows();
		}
	}
	
	
	public static void stopServer(String environment) {
		if(environment.contains("mac")){
			stopServerMac();
		}else{
			stopServerWindows();
		}
	} 
	
	public static void startServerWindows() {
		Runtime runtime = Runtime.getRuntime();
		try {
			runtime.exec("cmd.exe /c start cmd.exe /k \"appium -a 127.0.0.1 -p 4723 --session-override -dc \"{\"\"noReset\"\": \"\"false\"\"}\"\"");
			Thread.sleep(20000);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void stopServerWindows() {
		Runtime runtime = Runtime.getRuntime();
		try {
			runtime.exec("taskkill /FI \"WINDOWTITLE eq C:"+File.separator+"WINDOWS"+File.separator+"system32"+File.separator+"cmd.exe - appium*\" /F");
			Thread.sleep(2000);
			runtime.exec("taskkill /FI \"WINDOWTITLE eq C:"+File.separator+"WINDOWS"+File.separator+"system32"+File.separator+"cmd.exe - appium*\" /F");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 
	
	public static void startServerMac() {
		 
		CommandLine command = new CommandLine(
				"/usr/local/bin/node");
		command.addArgument(
				"/usr/local/bin/appium",
				false);
		command.addArgument("--address", false);
		command.addArgument("0.0.0.0");
		command.addArgument("--port", false);
		command.addArgument("4723");
		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		DefaultExecutor executor = new DefaultExecutor();
		executor.setExitValue(1);
		try {
			executor.execute(command, resultHandler);
			Thread.sleep(5000);
			System.out.println("Appium server started.");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
 
	public static void stopServerMac() {
		String[] command = { "/usr/bin/killall", "-KILL", "node" };
		try {
			Runtime.getRuntime().exec(command);
			System.out.println("Appium server stopped.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		AppiumUtil appiumServer = new AppiumUtil();
		appiumServer.startServer("win");
 
		appiumServer.stopServer("win");
	}
}
