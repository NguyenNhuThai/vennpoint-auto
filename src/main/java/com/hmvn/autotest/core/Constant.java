package com.hmvn.autotest.core;

public class Constant {

	public static String currentPlatform = "not set";
	public static String currentRuntime = "not set";

	//time limited for every test case run
	public static double LIMITED_TIME = 60000;

	// time wait for element
	public static int SMALL_WAITING_TIME = 3;

	public static int SMALL_NORMAL_WAITING_TIME = 5;

	public static int NORMAL_WAITING_TIME = 10;

	public static int LONG_WAITING_TIME = 15;
	
	public static int LONG_WAITING_TIME_30 = 30;

	//time for wait page load
	public static int PAGE_LOAD_TIMEOUT = 60;

	//for test link integration 
	public static String API_KEY_TESTLINK = "5e4627a51652f3c4b19aad10ed18eeda";

	// Substitute your Server URL Here
	public static String SERVER_URL = "http://10.220.48.92:8080/testlink/lib/api/xmlrpc/v1/xmlrpc.php";

	// Substitute your project name Here
	public static String PROJECT_NAME = "VENNPOINT";

	// Substitute your test plan Here
	public static String PLAN_NAME = "Daily Test";

	// Substitute your build name
	public static String BUILD_NAME = "not set";

	// Substitute your test suite name
	public static String TEST_SUITE = "not set";

	// Substitute your active
	public static String ACTIVE = "active";
	// Substitute your active
	public static String DISABLED = "disabled";
	
	public static String FOLDER_TEMPLATE_REPORT = "\\src\\main\\resources\\templateReport";

}
