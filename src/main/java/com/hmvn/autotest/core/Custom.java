package com.hmvn.autotest.core;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Custom {
	public static String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	public static String numbers = "0123456789";
	private static int SMALL_WAIT_TIME = 500;

	public static void tabElement(AppiumDriver driver, WebElement element){
		   TouchAction action = new TouchAction(driver);
	       action.tap(element);
	       action.waitAction(300);
	       action.perform();
	}
	
	public static String getCurrentDate() {
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		return simpleDateFormat.format(date);
	}

	public static void checkDivDangTaiDismis(WebDriver driver, int second) {
		int t = 0;
		List<WebElement> listWebElement = driver.findElements(By.xpath("//div[contains(text(),'�?ang tải')]"));
		if (listWebElement.size() > 0) {
			while (driver.findElement(By.xpath("//div[contains(text(),'�?ang tải')]")).isDisplayed() && t < second) {
				try {
					Thread.sleep(SMALL_WAIT_TIME);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				t = t + 1;
			}
		}
	}

	public static String getRandom12Number() {
		String alphabet = new String("0123456789");
		int n = alphabet.length();
		String result = new String();
		Random r = new Random();

		for (int i = 0; i < 12; i++)
			result = result + alphabet.charAt(r.nextInt(n));

		result = result + r.nextInt(9);
		return result;

	}

	public static String getRandom5Characters() {
		String alphabet = new String("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
		int n = alphabet.length();
		String result = new String();
		Random r = new Random();

		for (int i = 0; i < 5; i++)
			result = result + alphabet.charAt(r.nextInt(n));

		result = result + r.nextInt(9);
		return result;

	}

	public static String getRandomCharacters(int lenght) {
		String alphabet = new String(characters);
		int n = alphabet.length();
		String result = new String();
		Random r = new Random();

		for (int i = 0; i < lenght; i++) {
			result += alphabet.charAt(r.nextInt(n));
		}

		return result;
	}

	public static String getRandomNumbers(int lenght) {
		String alphabet = new String(numbers);
		int n = alphabet.length();
		String result = new String();
		Random r = new Random();

		for (int i = 0; i < lenght; i++) {
			result += alphabet.charAt(r.nextInt(n));
		}

		return result;
	}

	/**
	 * 
	 * @param driver
	 * @param xpath
	 * @param timeOut
	 * @return
	 */
	public static boolean checkElementExist(WebDriver driver, By by) {
		boolean result = false;
		int timeOut = Constant.LONG_WAITING_TIME;
		if (timeOut > 0) {
			timeOut = timeOut * 1000;
		}

		int time = 0;
		List<WebElement> els = new ArrayList<WebElement>();
		try {
			do {
				if (timeOut > 0 && time >= timeOut) {
					break;
				}
				Thread.sleep(100);
				time += 100;
				els = driver.findElements(by);
				if (!els.isEmpty()) {
					result = true;
				}
			} while (els.isEmpty());
		} catch (Exception e) {
		}

		return result;
	}

	/**
	 * 
	 * @param driver
	 * @param xpath
	 * @param timeOut
	 * @return
	 */
	public static boolean checkElementUnExist(WebDriver driver, By by) {
		boolean result = false;
		int timeOut = Constant.LONG_WAITING_TIME;
		if (timeOut > 0) {
			timeOut = timeOut * 1000;
		}

		int time = 0;
		List<WebElement> els = new ArrayList<WebElement>();
		try {
			do {
				if (timeOut > 0 && time >= timeOut) {
					break;
				}
				Thread.sleep(100);
				time += 100;
				els = driver.findElements(by);
				if (els.isEmpty()) {
					result = true;
				}
			} while (!els.isEmpty());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * 
	 * @return this method return random characters
	 */
	public String getRandomCharacters(String preFix, int numberCharacter, String afterFix, String characters) {
		String randomString = preFix + RandomStringUtils.random(numberCharacter, characters) + afterFix;

		return randomString;
	}

	public static boolean waitElementEnable(WebDriver driver, By by, int time) {
		boolean result = false;
		int i = 0;
		try {
			WebElement element = driver.findElement(by);
			while (!result && i < time) {
				try {
					element = driver.findElement(by);
					result = element.isEnabled();
					Thread.sleep(SMALL_WAIT_TIME);
					i = i + 1;
				} catch (Throwable e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * 
	 * @param driver
	 * @param element
	 * @param time
	 * @return
	 */
	public static boolean waitElementEnable(WebDriver driver, WebElement element, int time) {
		boolean result = false;
		int i = 0;
		try {
			while (!element.isEnabled() && i < time) {
				Thread.sleep(SMALL_WAIT_TIME);
				i = i + 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public static boolean waitElementDisable(WebDriver driver, WebElement el, int time) {
		boolean result = false;
		int i = 0;
		try {
			while (el.isDisplayed() && i < time) {
				Thread.sleep(SMALL_WAIT_TIME);
				i = i + 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * 
	 * @param driver
	 * @param locator
	 * @param second
	 * @return
	 */
	public static boolean waitElementDisplay(WebDriver driver, By locator, int second) {
		boolean result = false;
		int i = 0;
		while(i < second){
			try{
				driver.findElement(locator);
				result = true;
				break;
			} catch(Exception ex){
			}
			try {
				Thread.sleep(1000);
				i++;
			} catch (Exception e) {
			
			}
		}
		return result;
	}
	
	public static boolean waitElementDisplay(WebDriver driver, By locator) {
		boolean result = false;
		int i = 0;
		while(i < Constant.LONG_WAITING_TIME){
			try{
				driver.findElement(locator);
				result = true;
				break;
			} catch(Exception ex){
			}
			try {
				Thread.sleep(1000);
				i++;
			} catch (Exception e) {
			
			}
		}
		return result;
	}
	
	public static boolean waitElementVisibility(WebDriver driver, By locator) {
		boolean result = false;
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try{
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(locator)));
			result = true;
		} catch(Exception ex){
		}
		return result;
	}

	/**
	 * 
	 * @param driver
	 * @param locator
	 * @param time
	 * @return
	 */
	public static boolean waitElementNotDisplay(WebDriver driver, By locator, int time) {
		boolean result = false;
		try {
			List<WebElement> listWebElement = driver.findElements(locator);
			int i = 0;
			while (listWebElement.size() == 0 && i < time) {
				listWebElement = driver.findElements(locator);
				Thread.sleep(SMALL_WAIT_TIME);
				i = i + 1;
			}
			if (listWebElement.size() != 0) {
				if (!listWebElement.get(0).isDisplayed()) {
					result = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 
	 * @param driver
	 * @param locator
	 * @param time
	 * @return
	 */
	public static boolean waitElementDismis(WebDriver driver, By locator, int time) {
		boolean result = false;
		try {
			WebElement element = driver.findElement(locator);
			int i = 0;
			while(i < time){
				element = driver.findElement(locator);
				Thread.sleep(SMALL_WAIT_TIME);
				i++;
			}
		} catch (Exception e) {
//			e.printStackTrace();
			result = true;
		}
		return result;
	}

	/**
	 * 
	 * @param driver
	 * @param locator
	 */
	public static void waitUntilElementChanged(WebDriver driver, final By locator) {
		// convert xpath to css selector
		new WebDriverWait(driver, 180).until(new ExpectedCondition<Boolean>() {
			boolean firstTime = true;

			public Boolean apply(WebDriver driver) {
				WebElement element = driver.findElement(locator);
				if (element == null) {
					return false;
				}
				if (firstTime) {
					firstTime = false;
					((JavascriptExecutor) driver).executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);", element, "loading", "true");
					return false;
				}
				return (Boolean) (element.getAttribute("loading") == null);
			}
		});
	}

	/**
	 * 
	 * @param driver
	 * @param second
	 * @return
	 */
	public static boolean waitDismisElement(WebElement el, int second) {
		boolean result = false;
		try {
			for (int i = 0; i < second; i++) {
				if (!el.isDisplayed()) {
					result = true;
					break;
				} else {
					try {
						Thread.sleep(SMALL_WAIT_TIME);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	/**
	 * 
	 * @param driver
	 * @param second
	 * @return
	 */
	public static boolean waitDisplayedElement(WebDriver driver, String xpath, int second) {
		boolean result = false;

		for (int i = 0; i < second; i++) {
			try {
				WebElement el = driver.findElement(By.xpath(xpath));
				if (el != null && el.isDisplayed()) {
					result = true;
					break;
				}
				Thread.sleep(SMALL_WAIT_TIME);
			} catch (Exception e) {
			}
		}

		return result;
	}

	/**
	 * 
	 * @param driver
	 * @param second
	 * @return
	 */
	public static boolean waitDisplayedElement(WebElement el, int second) {
		boolean result = false;
		try {
			for (int i = 0; i < second; i++) {
				if (el.isDisplayed()) {
					result = true;
					break;
				} else {
					try {
						Thread.sleep(SMALL_WAIT_TIME);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return result;
	}
	
	public static boolean waitDisplayedElement(WebElement el) {
		boolean result = false;
		int second = Constant.LONG_WAITING_TIME;
		try {
			for (int i = 0; i < second; i++) {
				if (el.isDisplayed()) {
					result = true;
					break;
				} else {
					try {
						Thread.sleep(SMALL_WAIT_TIME);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return result;
	}

	/**
	 * 
	 * @param el
	 * @param second
	 * @return
	 */
	public static boolean waitChangeContentElement(WebElement el, int second) {
		boolean result = false;
		String oldContent = el.getText();
		try {
			for (int i = 0; i < second; i++) {
				if (!el.getText().equals(oldContent)) {
					result = true;
					break;
				} else {
					try {
						Thread.sleep(SMALL_WAIT_TIME);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	/**
	 * 
	 * @param driver
	 * @param xpath
	 * @param number
	 * @param timeSecond
	 * @param waitElement
	 *            is tagName
	 * @return
	 */
	public static boolean waitCompleteLoadElement(WebDriver driver, String xpath, String compareValue, int timeSecond) {
		boolean result = false;
		for (int i = 0; i < timeSecond; i++) {
			try {
				if (driver.findElements(By.xpath(xpath)).size() > 0 && driver.findElement(By.xpath(xpath)).getText().contains(compareValue)) {
					result = true;
					break;
				}
				Thread.sleep(SMALL_WAIT_TIME);
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
		}
		return result;
	}

	public static boolean waitElementVisible(WebDriver driver, String xpath, int timeSecond) {
		boolean result = false;
		for (int i = 0; i < timeSecond; i++) {
			try {
				Thread.sleep(SMALL_WAIT_TIME);
				driver.findElement(By.xpath(xpath));
				result = true;
				break;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public static boolean checkMessagePopupMCDV(WebDriver driver, String id, String msg, int timeSecond) {
		boolean result = false;
		for (int i = 0; i < timeSecond; i++) {
			try {
				((JavascriptExecutor) driver).executeScript("$('#noty_temp').text()");
				Thread.sleep(SMALL_WAIT_TIME);
			} catch (Exception e) {
				System.out.println("wait : " + e.getMessage());
			}
		}
		return result;
	}

	public static String getDirFolder(String folderName) {
		// Custom.getDirFolder(com.vinecom.adayroi.core.Constant.FOLDER_AUTOIT_APP)
		String dir = folderName + File.separator;
		if (!PropertiesHelper.getKey("project.basedir").isEmpty()) {
			dir = PropertiesHelper.getKey("project.basedir") + File.separator + folderName + File.separator;
		}

		return dir;
	}

	/**
	 * 
	 * @param driver
	 * @param by
	 * @param time seconds
	 * @return
	 */
	public static boolean clickElement(WebDriver driver, By by, int time) {
		boolean result = false;
		time = time * 1000;
		try {

			do {
				if (time <= 0) {
					break;
				}
				try {
					driver.findElement(by).click();
					result = true;
				} catch (Exception e) {
				}

				Thread.sleep(1000);
				time -= 1000;
			} while (!result);
		} catch (Exception e) {
		}

		return result;
	}

	/**
	 * 
	 * @param el
	 * @param time
	 * @return
	 */
	public static boolean clickElement(WebElement el, int time) {
		boolean result = false;
		try {

			do {
				if (time <= 0) {
					break;
				}
				try {
					el.click();
					result = true;
				} catch (Exception e) {
				}

				Thread.sleep(500);
				time -= 500;
			} while (!result);
		} catch (Exception e) {
			// TODO: handle exception
		}

		return result;
	}
	
	/**
	 * 
	 * @param el
	 * @return
	 */
	public static boolean clickElement(WebElement el) {
		boolean result = false;
		int time = Constant.LONG_WAITING_TIME;
		try {

			do {
				if (time <= 0) {
					break;
				}
				try {
					el.click();
					result = true;
				} catch (Exception e) {
				}

				Thread.sleep(500);
				time -= 500;
			} while (!result);
		} catch (Exception e) {
			// TODO: handle exception
		}

		return result;
	}
	
	/**
	 * 
	 * @param string
	 */
	public static void luuLog(String logFileDir, String string) {
		try {
			File logFile = new File(logFileDir);
			if (logFile.exists()) {
				logFile.createNewFile();
			}

			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(logFile), "UTF-8"));
			writer.write(string);
			writer.close();
		} catch (Exception e) {
		}
	}
	
	public void logTrace(String logFolderPath, String logFilePath, String log) throws Exception{
		File logFolder = new File(logFolderPath);
		File logFile = new File(logFilePath);
		
		try{
			if(!logFolder.isDirectory() || !logFolder.exists()){
				logFolder.mkdir();
				logFile.createNewFile();
			}
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(logFile, true)));
			writer.println(log);
			writer.flush();
			writer.close();
		} catch(Exception ex){
			ex.printStackTrace();
		}
	}

    public static boolean waitForElementToBeVisibleOrNot(WebDriver driver, final By locator) {
        final WebDriverWait wait = new WebDriverWait(driver, Constant.LONG_WAITING_TIME);
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            return true;
        } catch (final RuntimeException exception) {
            return false;
        }
    }
    
    public static void waitForElementToBeClickable(WebDriver driver, final By locator) {
        final WebDriverWait wait = new WebDriverWait(driver, Constant.LONG_WAITING_TIME*1000);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }
    
}
