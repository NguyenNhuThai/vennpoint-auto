package com.hmvn.autotest.core;

import java.util.HashMap;

public class DataProvider {
	
	public static HashMap<String, String> mcData;
	public static HashMap<String, String> csData;
	
	public static void init(){
		System.out.println("init");
		mcData = new HashMap<String, String>();
		csData = new HashMap<String, String>();
	}
	
	public static void putData(String tcName, String orderCode){
		System.out.println(tcName + " " + orderCode);
		mcData.put(tcName, orderCode);
	}
	
	public static String getData(String tcName){
		return mcData.get(tcName);
	}
	
	
}
