package com.hmvn.autotest.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {
	private static XSSFSheet ExcelWSheet;
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell Cell;
	private static String reportFilePath = PropertiesHelper.getKey("project.basedir");

	/**
	 * 
	 * @param TCname
	 * @return get row input data by tc name
	 */
	public static int getRowByTCName(String TCname) {
		int latRowIndex = getLastRowIndex();
		int row = -1;
		for (int i = 0; i <= latRowIndex; i++) {
			if (getCellData(i, 0).trim().equals(TCname.trim())) {
				row = i;
				break;
			}
		}
		return row;
	}
	
	public static int getRowByTCNameColumnIndex(String TCname, int columnIndex) {
		int latRowIndex = getLastRowIndex();
		int row = -1;
		for (int i = 0; i <= latRowIndex; i++) {
			if (getCellData(i, columnIndex).trim().equals(TCname.trim())) {
				row = i;
				break;
			}
		}
		return row;
	}
	
	/**
	 * 
	 * @param TCname
	 * @param columnIndex
	 * @return
	 */
	public static ArrayList<Integer> getRowsByTCNameColumnIndex(String TCname, int columnIndex) {
		int latRowIndex = getLastRowIndex();
		ArrayList<Integer> rows = new ArrayList<Integer>();
		for (int i = 0; i <= latRowIndex; i++) {
			if (getCellData(i, columnIndex).trim().equals(TCname.trim())) {
				rows.add(i);
			}
		}
		return rows;
	}

	public static XSSFSheet getSheet(){
		return ExcelWSheet;
	}
	public static XSSFWorkbook getWorkbook(){
		return ExcelWBook;
	}
	// this method is to get the path of the file 'test.xlsx' in folder driver
	// This method is to set the File path and to open the Excel file, Pass
	// Excel Path and Sheetname as Arguments to this method
	public static void setExcelFile(String Path, String SheetName) {
		try {
			File file = new File(Path);
			System.out.println("path file " + file.getAbsolutePath());
			if (file.exists() && file.isFile()) {
				// Open the Excel file
				FileInputStream ExcelFile = new FileInputStream(Path);
				// Access the required test data sheet
				ExcelWBook = new XSSFWorkbook(ExcelFile);
				ExcelWSheet = ExcelWBook.getSheet(SheetName);
				System.out.println("sheet name " + ExcelWSheet.getSheetName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	// this method is to get the path of the file 'test.xlsx' in folder driver
	// This method is to set the File path and to open the Excel file, Pass
	// Excel Path and Sheetname as Arguments to this method
	public static void setExcelFile(String Path, int indexSheet) {
		try {
			File file = new File(Path);
			System.out.println("path file " + file.getAbsolutePath());
			if (file.exists() && file.isFile()) {
				// Open the Excel file
				FileInputStream ExcelFile = new FileInputStream(Path);
				// Access the required test data sheet
				ExcelWBook = new XSSFWorkbook(ExcelFile);
				ExcelWSheet = ExcelWBook.getSheetAt(indexSheet);
				System.out.println("sheet name " + ExcelWSheet.getSheetName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// This method is to read the test data from the Excel cell, in this we are
	// passing parameters as Row num and Col num
	public static String getCellData(int RowNum, int ColNum) {
		try {
			String CellData = "";
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			if (Cell == null) {
				return CellData;
			}
			
			switch (Cell.getCellType()) {
			case 0:
				CellData = String.valueOf(Cell.getNumericCellValue());
				break;
			case 1:
			case 2:
				CellData = Cell.getStringCellValue();
				break;
			case 3:
				CellData = String.valueOf(Cell.getNumericCellValue());
				break;
			// case 4:
			// CellData = String.valueOf(Cell.getNumericCellValue());
			// break;
			// case 5:
			// CellData = String.valueOf(Cell.getNumericCellValue());
			// break;
			default:
				CellData = Cell.getStringCellValue();
				break;
			}

			if (CellData.trim().equals("0.0")) {
				CellData = "";
			}
			if (CellData.contains(".0")) {
				CellData = CellData.replace(".0", "");
			}
			CellData = CellData.replace(String.valueOf((char) 160), " ");
			return CellData.trim();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	// This method is to read the test data from the Excel cell, in this we are
		// passing parameters as Row num and Col num
		public static boolean getCellDataBoolean(int RowNum, int ColNum) {
			boolean result = false; 
			try {
				String CellData = "";
				Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
				if (Cell == null) {
					return false;
				}
				
				switch (Cell.getCellType()) {
				case 0:
					CellData = String.valueOf(Cell.getNumericCellValue());
					break;
				case 1:
				case 2:
					CellData = Cell.getStringCellValue();
					break;
				case 3:
					CellData = String.valueOf(Cell.getNumericCellValue());
					break;
				// case 4:
				// CellData = String.valueOf(Cell.getNumericCellValue());
				// break;
				// case 5:
				// CellData = String.valueOf(Cell.getNumericCellValue());
				// break;
				default:
					CellData = Cell.getStringCellValue();
					break;
				}
				
				CellData = CellData.replace(String.valueOf((char) 160), " ");

				if (CellData.trim().equals("x")) {
					return true;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
		}

	/**
	 * 
	 * @param cell
	 * @param rowNum
	 * @param colNum
	 * @return
	 */
	public static String getCellData(XSSFSheet excelWSheet, int rowNum, int colNum) {
		try {
			String CellData = "";
			XSSFCell cell = excelWSheet.getRow(rowNum).getCell(colNum);
			if (cell == null) {
				return CellData;
			}

			switch (cell.getCellType()) {
			case 0:
				CellData = String.valueOf(cell.getNumericCellValue());
				break;
			case 1:
			case 2:
				CellData = cell.getStringCellValue();
				break;
			case 3:
				CellData = String.valueOf(cell.getNumericCellValue());
				break;
			// case 4:
			// CellData = String.valueOf(Cell.getNumericCellValue());
			// break;
			// case 5:
			// CellData = String.valueOf(Cell.getNumericCellValue());
			// break;
			default:
				CellData = cell.getStringCellValue();
				break;
			}

			if (CellData.trim().equals("0.0")) {
				CellData = "";
			}
			
			if (CellData.trim().contains(".0")) {
				CellData = CellData.replace(".0", "");
			}

			CellData = CellData.replace(String.valueOf((char) 160), " ");
			return CellData.trim();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public static void createRow(int rowNum){
		ExcelWSheet.createRow(rowNum);
	}
	public static void removeRow(int rowNum){
		XSSFRow row = ExcelWSheet.getRow(rowNum);
		ExcelWSheet.removeRow(row);
	}
	public static int getLastColumnIndex() {
		XSSFRow row = ExcelWSheet.getRow(0);
		return row.getLastCellNum();
	}
	public static int getLastColumnIndex(int rowIndex) {
		XSSFRow row = ExcelWSheet.getRow(rowIndex);
		return row.getLastCellNum();
	}
	public static int getLastRowIndex() {
		return ExcelWSheet.getLastRowNum();
	}

	public static String getPathExcel(String name) {
		return PropertiesHelper.getKey("project.basedir") + File.separator + "resources\\input-data-excel" + File.separator + name;
	}

	public static void main(String arg[]) {
		String path = PropertiesHelper.getKey("project.basedir") + File.separator + "resources\\input-data-excel" + File.separator + "MC.xlsx";
		try {
			setExcelFile(path, "Sheet1");
			setCellData("fail", 2, 2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param result
	 * @param RowNum
	 * @param colNum
	 */
	public static void setCellData(String result, int rowNum, int colNum) {
		try {
			XSSFCell cell = ExcelWSheet.getRow(rowNum).createCell(colNum);
			cell.setCellType(XSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param excelSheet
	 * @param result
	 * @param rowNum
	 * @param colNum
	 */
	public static void setCellData(XSSFSheet excelSheet, String result, int rowNum, int colNum) {
		try {
			XSSFCell cell = excelSheet.getRow(rowNum).createCell(colNum);
			cell.setCellType(XSSFCell.CELL_TYPE_STRING);
			cell.setCellValue(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param function
	 */
	public static void saveExcelFile(String function) {
		try {
			Date date = new Date();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
			reportFilePath = reportFilePath + function + "-" + simpleDateFormat.format(date) + ".xlsx";
			File file = new File(reportFilePath);
			if (!file.exists()) {
				file.createNewFile();
			}

			FileOutputStream out = new FileOutputStream(reportFilePath);
			ExcelWBook.write(out);
			out.flush();
			out.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param function
	 */
	public static void saveExcelFileByPath(String path) {
		System.out.println("saving path : " + path);
		try {
			File file = new File(path);
			if (!file.exists()) {
				file.createNewFile();
			}

			FileOutputStream out = new FileOutputStream(path);
			ExcelWBook.write(out);
			out.flush();
			out.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param excelWBook
	 * @param path
	 */
	public static void saveExcelFileByPath(XSSFWorkbook excelWBook, String path) {
		try {
			File file = new File(path);
			if (!file.exists()) {
				file.createNewFile();
			}

			FileOutputStream out = new FileOutputStream(path);
			excelWBook.write(out);
			out.flush();
			out.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param function
	 */
	public static void saveExcelFileByTs(String testSuite) {
		try {
			Date date = new Date();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
			String pathFolder = PropertiesHelper.getKey("project.basedir") + File.separator + "report";
			File folderReport = new File(pathFolder);
			if (!folderReport.exists()) {
				folderReport.mkdir();
			}
			reportFilePath = pathFolder + File.separator + testSuite + "-" + simpleDateFormat.format(date) + ".xlsx";
			File file = new File(reportFilePath);
			System.out.println(reportFilePath);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream out = new FileOutputStream(reportFilePath);
			ExcelWBook.write(out);
			out.flush();
			out.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param function
	 */
	public static void saveExcelFileByTsAndBrowser(String testSuite, String browser) {
		try {
			Date date = new Date();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
			String pathFolder = PropertiesHelper.getKey("project.basedir") + File.separator + "report";
			File folderReport = new File(pathFolder);
			if (!folderReport.exists()) {
				folderReport.mkdir();
			}
			reportFilePath = pathFolder + File.separator + testSuite + "-" + simpleDateFormat.format(date) + " " + browser + ".xlsx";
			File file = new File(reportFilePath);
			System.out.println(reportFilePath);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileOutputStream out = new FileOutputStream(reportFilePath);
			ExcelWBook.write(out);
			out.flush();
			out.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param cellValue
	 */
	public static String getNumberColumnByName(String cellValue) {
		String numberColumn = "khongTonTai";
		try {
			// outerloop:
			for (int i = 0; i <= 2; i++) {
				XSSFRow row = ExcelWSheet.getRow(i);
				for (int j = 0; j <= row.getLastCellNum(); j++) {
					XSSFCell cell = row.getCell(j);
					if (cell != null) {
						if ((cell.getCellType() == 2 || cell.getCellType() == 1) && cell.getStringCellValue().equals(cellValue)) {
							if (!numberColumn.equals("khongTonTai") && cell.getColumnIndex() > Integer.valueOf(numberColumn)) {
								numberColumn = String.valueOf(cell.getColumnIndex());
							} else if (numberColumn.equals("khongTonTai")) {
								numberColumn = String.valueOf(cell.getColumnIndex());
								// break outerloop;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return numberColumn;
	}

	/**
	 * 
	 * @param text
	 * @return
	 */
	public static int getRowIndexByText(String text) {
		System.out.println("text : " + text);
		int numberRow = 0;
		try {
			System.out.println("sheet name: " + ExcelWSheet.getSheetName());
			int lastRow = getLastRowIndex();
			System.out.println("last row " + lastRow);
			for (int i = 0; i <= lastRow; i++) {
				XSSFRow row = ExcelWSheet.getRow(i);
				for (int j = 0; j <= row.getLastCellNum(); j++) {
					XSSFCell cell = row.getCell(j);
					if (cell != null) {
						if ((cell.getCellType() == 2 || cell.getCellType() == 1) && cell.getStringCellValue().equals(text)) {
							if (row.getRowNum() > numberRow) {
								numberRow = row.getRowNum();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return numberRow;
	}

	
	/**
	 * 
	 * @param text
	 * @param columnNumber
	 * @return
	 */
	public static int getRowIndexByTextAndColumn(String text, int columnNumber) {
		System.out.println("text : " + text);
		int numberRow = 0;
		try {
			System.out.println("sheet name: " + ExcelWSheet.getSheetName());
			int lastRow = getLastRowIndex();
			System.out.println("last row " + lastRow);
			for (int i = 0; i <= lastRow; i++) {
				XSSFRow row = ExcelWSheet.getRow(i);
				XSSFCell cell = row.getCell(columnNumber);
				if (cell != null) {
					if ((cell.getCellType() == 2 || cell.getCellType() == 1) && cell.getStringCellValue().equals(text)) {
						if (row.getRowNum() > numberRow) {
							numberRow = row.getRowNum();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return numberRow;
	}

	/**
	 * 
	 * @param columnIndex
	 * @return
	 */
	public static int getRowIndexOfFirstEmptyCell(XSSFSheet excelSheet, int columnIndex) {
		try {
			int lastRow = excelSheet.getLastRowNum();
			for (int i = 1; i <= lastRow; i++) {
				XSSFRow row = excelSheet.getRow(i);
				XSSFCell cell = row.getCell(columnIndex);
				if (cell == null || cell.getStringCellValue().isEmpty()) {
					return row.getRowNum();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return -1;
	}
}
