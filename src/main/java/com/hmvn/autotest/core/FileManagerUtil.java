package com.hmvn.autotest.core;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.testng.ISuite;

import io.appium.java_client.AppiumDriver;

public class FileManagerUtil {
	private static File reportDirectory;
	private static File reportFile;
	private static String severReportDirectoryPath;
	public static HashMap<String, String> severReportDirectoryPaths = new HashMap<String, String>();
	public static HashMap<String, File> reportDirectories = new HashMap<String, File>();
	
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	public static SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
	public static Date currentDate = new Date();

	// excel util
	private static String excelInputPath = "";
	private static String excelOutputPath = "";

	public static void makeReportDirectory(String folderName) {
		System.out.println(folderName);
		
		String path = PropertiesHelper.getKey("project.basedir") + File.separator + "report" + File.separator + dateFormat.format(currentDate) + File.separator
				+ folderName + "_" + timeFormat.format(currentDate);
		reportDirectory = new File(path);
		if (!reportDirectory.exists()) {
			reportDirectory.mkdirs();
			System.out.println("Make dirs: " + reportDirectory);
		} else {
			System.out.println("Dirs exists: " + reportDirectory);
			path += "_" + UUID.randomUUID().toString();
			reportDirectory = new File(path);
			System.out.println("New dirs: " + reportDirectory);
		}
		reportDirectories.put(folderName, reportDirectory);
		severReportDirectoryPath = dateFormat.format(currentDate) + File.separator + folderName + "_" + timeFormat.format(currentDate);
		severReportDirectoryPaths.put(folderName, severReportDirectoryPath);
	}

	public static void makeLogFile() {
		try {
			String logFilePath = reportDirectory + File.separator + "log.txt";
			PrintStream out = new PrintStream(new FileOutputStream(logFilePath));
			System.setOut(out);
			System.setErr(out);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void setExcelFile(String suiteName, int sheetIndex) {
		excelInputPath = PropertiesHelper.getKey("project.basedir") + File.separator + "resources" + File.separator + "input-data-excel" + File.separator
				+ "Data-" + suiteName + ".xlsx";
		ExcelUtil.setExcelFile(excelInputPath, sheetIndex);
	}

	public static void saveExcelFile(String suiteName) {
		excelOutputPath = reportDirectory + File.separator + suiteName + "-" + dateFormat.format(currentDate) +  ".xlsx";
		ExcelUtil.saveExcelFileByPath(excelOutputPath);
	}

	public static String convertImageToBase64(File file){
		BufferedImage img = null;
		String imageString = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
		try {
			img = ImageIO.read(file);
			ImageIO.write(img, "png", bos);
			byte[] imageBytes = bos.toByteArray();
			imageString = Base64.getEncoder().encodeToString(imageBytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return imageString;
	}
	
	public static String takeSnapshot(String fileName) {
		System.out.println("takeSnapshot " + fileName);
		String filePath = reportDirectory + File.separator + fileName + ".png";
		File snapshotFile = new File(filePath);
		try {
			System.out.println("Snapshot file: " + snapshotFile);
			System.out.println("current platform " + Constant.currentPlatform);

			if (Constant.currentPlatform.equalsIgnoreCase("web")) {
				File output = ((TakesScreenshot) DriverUtil.getCurrentDriver()).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(output, snapshotFile);
			}
			else if(Constant.currentPlatform.contains("mobile")){
				AppiumDriver driver = AppiumUtil.getCurrentDriver();
				String currentContext = driver.getContext();
				Set<String> contextNames = driver.getContextHandles();
				driver.context("NATIVE_APP");
				 File output = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		        FileUtils.copyFile(output, snapshotFile);
		    	driver.context(currentContext);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return(convertImageToBase64(snapshotFile));
	}

	public static String getSnapshotFilePath(String fileName) {
		return fileName + ".png";
	}

	public static String getServerSnapshotFilePath(String suiteName, String fileName) {
		return severReportDirectoryPaths.get(suiteName) + File.separator + fileName + ".png";
	}

	public static void exportHtmlReport(String html, ISuite suite) {
		try {
			reportFile = new File(reportDirectories.get(suite.getName()) + File.separator + "report.html");
			FileUtils.writeStringToFile(reportFile, html, "UTF-8");
			System.out.println("Report file: " + reportFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void openReportFile() {
		try {
			Desktop desktop = Desktop.getDesktop();
			desktop.open(reportFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void zipReportDirectory() {
		try {
			zipDirectory(reportDirectory);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void zipDailyReportDirectory() {
		try {
			zipDirectory(reportDirectory.getParentFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static List<File> fileList;

	private static void zipDirectory(File directoryToZip) throws IOException {
		fileList = new ArrayList<File>();
		addFiles(directoryToZip);

		String zipFilePath = directoryToZip + ".zip";
		FileOutputStream fos = new FileOutputStream(zipFilePath);
		ZipOutputStream zos = new ZipOutputStream(fos);
		byte[] buffer = new byte[1024];

		for (File file : fileList) {
			String filePath = file.getAbsolutePath();
			String zipEntryName = filePath.substring(directoryToZip.getAbsolutePath().length() + 1, filePath.length());
			ZipEntry ze = new ZipEntry(zipEntryName);
			zos.putNextEntry(ze);
			FileInputStream fis = new FileInputStream(filePath);

			int len;
			while ((len = fis.read(buffer)) > 0) {
				zos.write(buffer, 0, len);
			}
			zos.closeEntry();
			fis.close();
		}
		zos.close();
		fos.close();
	}
	public static void copyReportToServer(String suiteName) {
		System.out.println("suite name " + suiteName);
		try {
			String serverReportDir = PropertiesHelper.getKey("server.report.directory");
			File autoTeamFile = new File(serverReportDir);
			if (!autoTeamFile.exists()) {
				System.out.println("Can't found server image folder");
			} else {
				String serverDailyReportDir = serverReportDir + severReportDirectoryPaths.get(suiteName);
				File file = new File(serverDailyReportDir);
				FileUtils.copyDirectory(reportDirectories.get(suiteName), file);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private static void addFiles(File dir) throws IOException {
		File[] files = dir.listFiles();
		for (File file : files) {
			if (file.isFile()) {
				String ext = FilenameUtils.getExtension(file.getAbsolutePath());
				if (!"zip".equals(ext)) {
					fileList.add(file);
				}
			} else {
				addFiles(file);
			}
		}
	}
}
