package com.hmvn.autotest.core;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class HttpUtil {
	private static int SMALL_WAIT_TIME = 500;

	/**
	 * 
	 * @param url
	 * @param data
	 */
	public static boolean postData(String url, HashMap<String, String> data) {
		boolean success = false;
		String result = "";
		try {
			HttpClient httpclient;
			HttpPost httppost;
			ArrayList<NameValuePair> postParameters;
			httpclient = new DefaultHttpClient();
			httppost = new HttpPost(url);
			postParameters = new ArrayList<NameValuePair>();

			for (String key : data.keySet()) {
				//System.out.println("key " + key + ", value " + data.get(key));
				postParameters.add(new BasicNameValuePair(key, data.get(key)));
			}
			UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(postParameters, "UTF-8");
			httppost.setEntity(urlEncodedFormEntity);
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();
			InputStream is = entity.getContent();
			// Read response to string
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				is.close();
				result = sb.toString();
				System.out.println("result string " + result);
			} catch (Exception e) {
				e.printStackTrace();
			}

			System.out.println("status line " + response.getStatusLine());

			Thread.sleep(SMALL_WAIT_TIME);
			
			if (response.getStatusLine().equals("HTTP/1.1 200 OK")) {
				success = true;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return success;
	}
	
	public static boolean postString(String url, String data) {
		boolean success = false;
		String result = "";
		try {
			HttpClient httpclient;
			HttpPost httppost;
			httpclient = new DefaultHttpClient();
			httppost = new HttpPost(url);

			StringEntity stringEntity = new StringEntity(data, ContentType.APPLICATION_JSON);
			httppost.setEntity(stringEntity);
			HttpResponse response = httpclient.execute(httppost);

			HttpEntity entity = response.getEntity();
			InputStream is = entity.getContent();
			// Read response to string
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				is.close();
				result = sb.toString();
				System.out.println("result string " + result);
			} catch (Exception e) {
				e.printStackTrace();
			}

			System.out.println("status line " + response.getStatusLine());

			Thread.sleep(SMALL_WAIT_TIME);
			
			if (response.getStatusLine().equals("HTTP/1.1 200 OK")) {
				success = true;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return success;
	}
}
