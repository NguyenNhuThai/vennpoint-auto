package com.hmvn.autotest.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.openqa.selenium.remote.JsonException;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class Json {
	public static final String PROP_PROJECT_BASE_DIR = "project.basedir";
	public static final String PROP_DATA_DIR = "project.datadir";

	public static String getString(String url, String method) {
		InputStream is = null;
		String result = "";

		// HTTP
		try {
			HttpClient httpclient = new DefaultHttpClient(); // for port 80 requests!
			HttpResponse response = null;
			if (method.equals("GET")) {
				HttpGet httpGet = new HttpGet(url);
				response = httpclient.execute(httpGet);
			} else if (method.equals("")) {
				HttpPost httpPost = new HttpPost(url);
				response = httpclient.execute(httpPost);
			}
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		} catch (Exception e) {
			// e.printStackTrace();
		}

		// Read response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
			System.out.println("result string: " + result);
		} catch (Exception e) {
			// e.printStackTrace();
		}

		return result;
	}

	public static JsonArray getJson(String url, String method) {
		JsonArray jsonArray = null;
		String result = getString(url, method);

		// Convert string to object
		try {
			JsonElement jsonElement = new JsonParser().parse(result);
			if (jsonElement.isJsonArray()) {
				jsonArray = (JsonArray) jsonElement;
				// System.out.println(jsonArray.toString());
			}
		} catch (JsonException e) {
			// e.printStackTrace();
		}

		return jsonArray;
	}

	/**
	 * 
	 * @param url
	 * @param method
	 * @return
	 */
	public static JsonElement getJsonElement(String url, String method) {
		JsonElement jsonElement = null;
		String result = getString(url, method);
		// Convert string to object
		try {
			jsonElement = new JsonParser().parse(result);
		} catch (JsonException e) {
			e.printStackTrace();
			return null;
		}

		return jsonElement;
	}
	
	public static JsonObject getJsonData(String dataName){
		JsonObject jsonObject = null;
		if(!dataName.contains(".json")) dataName = dataName + ".json";
		String path = PropertiesHelper.getKey(PROP_PROJECT_BASE_DIR) + PropertiesHelper.getKey(PROP_DATA_DIR) + File.separator + dataName;
		Object obj;
		try {
			obj = new JsonParser().parse(new FileReader(path));
	        jsonObject = (JsonObject) obj;
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
}
