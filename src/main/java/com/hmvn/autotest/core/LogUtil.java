package com.hmvn.autotest.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogUtil {
	
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");
	private static Date date = new Date();
	
	private static File logFolder = null;
	private static File logFile = null;
	
	private static StringBuilder logString;
	
	public static void init(String suiteName){
		String logFolderPath = Custom.getDirFolder("output") + suiteName + File.separator + dateFormat.format(date);
		String logFilePath =  logFolderPath + File.separator + suiteName + "-" + timeFormat.format(date) + ".txt";
		System.out.println("Log folder path : " + logFolderPath);
		System.out.println("Log file path : " + logFilePath);
		
		logFolder = new File(logFolderPath);
		logFile = new File(logFilePath);
		
		logString = new StringBuilder();
	}
	
	public static boolean isFirstWriteLog = true;
	
	public static void logTrace(String log){
		if(isFirstWriteLog){
			logString.append(log);
			isFirstWriteLog = false;
		}
		else{
			logString.append("\n"+log);
		}
	}
	
	
	public static void writeLog(){
		try{
			if(!logFolder.isDirectory() || !logFolder.exists()){
				logFolder.mkdirs();
			}
			if(!logFile.exists()){
				logFile.createNewFile();
			}
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(logFile), StandardCharsets.UTF_8));
			PrintWriter writer = new PrintWriter(bw);
			
			writer.println(logString);
			writer.flush();
			writer.close();
		} catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
