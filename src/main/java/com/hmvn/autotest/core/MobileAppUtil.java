package com.hmvn.autotest.core;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

import java.util.HashMap;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class MobileAppUtil {
	
	/**
	 * swipe screen for mobile 
	 */
	
	public static void swipeUpToDown(RemoteWebDriver driver){
		
			System.out.println(driver.manage().window().getSize().getHeight() +" height");
		    System.out.println(driver.manage().window().getSize().getWidth() +" width");
		    
		    Dimension mobileSize = driver.manage().window().getSize(); 
		    
		    JavascriptExecutor js = (JavascriptExecutor)driver;
	        HashMap<String, Double> swipeObject = new HashMap<String, Double>();
	        swipeObject.put("startX", mobileSize.getHeight() * 0.5);
	        swipeObject.put("startY", mobileSize.getWidth() * 0.2);
	        swipeObject.put("endX", mobileSize.getHeight() * 0.1);
	        swipeObject.put("endY", mobileSize.getWidth() * 0.2);
	        swipeObject.put("duration", 5.0);
	        try{
	            js.executeScript("mobile: swipe", swipeObject);
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
	}
	
	public static void swipe_up(RemoteWebDriver driver) {
		  JavascriptExecutor js = (JavascriptExecutor) driver;
		  HashMap<String, Double> swipeObject = new HashMap<String, Double>();  
		  swipeObject.put("startX", 0.5);  
		  swipeObject.put("startY", 0.3);
		  swipeObject.put("endX", 0.5);
		  swipeObject.put("endY", 0.9);
		  swipeObject.put("duration", 3.0);
		  js.executeScript("mobile: swipe", swipeObject);
	}
	 
	public static void swipe_down(RemoteWebDriver driver) {
		  JavascriptExecutor js = (JavascriptExecutor) driver;
		  HashMap<String, Double> swipeObject = new HashMap<String, Double>();  
		  swipeObject.put("startX", 0.5);  
		  swipeObject.put("startY", 0.9);
		  swipeObject.put("endX", 0.5);
		  swipeObject.put("endY", 0.1);
		  swipeObject.put("duration", 1.0);
		  js.executeScript("mobile: swipe", swipeObject);
	}
	
	public static void swipeBottomUp(AppiumDriver<MobileElement> driver, WebElement scrollToElement){
		
//		Actions actions = new Actions(driver);
//		
//	    actions.moveToElement(scrollToElement);
//	    actions.perform();
//	    try{
//	    	Thread.sleep(2000);
//	    } catch(Exception ex){
//	    	ex.printStackTrace();
//	    }
		
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		HashMap<String, String> scrollDownObject = new HashMap<String, String>();
		scrollDownObject.put("direction", "down");
		
		executor.executeScript("mobile: scroll", scrollDownObject);
		
		
//		driver.context("NATIVE_APP");
//		Dimension mobileSize = driver.manage().window().getSize();
//		
//		System.out.println("Width : " + mobileSize.getWidth());
//		System.out.println("Height : " + mobileSize.getHeight());
//		
//		int startx = mobileSize.getWidth() / 2;
//		int endx = startx;
//		int starty = (int) (mobileSize.getHeight() * 0.5);
//		int endy = 0;
//		
//		System.out.println("Start X : " + startx);
//		System.out.println("End X : " + endx);
//		System.out.println("Start Y : " + starty);
//		System.out.println("End Y : " + endy);
//		
//		
//		driver.swipe(startx, starty, endx, endy, 2);
		
		
		
		/*System.out.println(driver.manage().window().getSize().getHeight() +" height");
	    System.out.println(driver.manage().window().getSize().getWidth() +" width");
	    JavascriptExecutor js = (JavascriptExecutor)driver;
        HashMap<String, Double> swipeObject = new HashMap<String, Double>();
        swipeObject.put("startX", 0.3);
        swipeObject.put("startY", 0.5);
        swipeObject.put("endX", 0.6);
        swipeObject.put("endY", 0.5);
        swipeObject.put("duration", 5.0);
        try{
            js.executeScript("mobile: swipe", swipeObject);
        }catch(Exception e){
        	e.printStackTrace();
        }*/
		
		}
	
}
