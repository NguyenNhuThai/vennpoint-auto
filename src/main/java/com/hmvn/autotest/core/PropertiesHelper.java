package com.hmvn.autotest.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesHelper {
	public static final String DEFAULT_PROPERTIES = "system.properties";
	public static final String API_PROPERTIES = "api.properties";

	private static Properties prod;
	private static Properties apiProd;

	
	public static Properties getProperties() {
		if (prod == null) {
			prod = new Properties();
			try {
//				prod.load(PropertiesHelper.class.getClassLoader().getResourceAsStream(DEFAULT_PROPERTIES));
				prod.load(new FileInputStream(System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + DEFAULT_PROPERTIES));
			} catch (IOException e) {
				System.out.println("Not found system properties file");
			}
		}
		return prod;
	}
	
	public static Properties getApiProperties() {
		if (apiProd == null) {
			apiProd = new Properties();
			try {
//				prod.load(PropertiesHelper.class.getClassLoader().getResourceAsStream(DEFAULT_PROPERTIES));
				apiProd.load(new FileInputStream(System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + API_PROPERTIES));
			} catch (IOException e) {
				System.out.println("Not found system properties file");
			}
		}
		return apiProd;
	}

	public static String getKey(String key) {
		String value = "";
		try {
			Object obj = getProperties().get(key);
			if (obj != null) {
				value = obj.toString();
			}
		} catch (Exception e) {

		}
		return value;
	}
	
	public static String getApi(String key) {
		String value = "";
		try {
			Object obj = getApiProperties().get(key);
			if (obj != null) {
				value = obj.toString();
			}
		} catch (Exception e) {

		}
		return value;
	}
}
