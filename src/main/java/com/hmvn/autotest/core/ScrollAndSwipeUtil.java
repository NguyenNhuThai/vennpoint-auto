package com.hmvn.autotest.core;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;

public class ScrollAndSwipeUtil {
	public static void scrollToElement(WebDriver driver, WebElement el) {
	    if (driver instanceof JavascriptExecutor) {
	        ((JavascriptExecutor) driver)
	            .executeScript("arguments[0].scrollIntoView(true);", el);
	    }
	}
	
	public static void scrollTo(WebDriver driver, int x, int y) {
	    if (driver instanceof JavascriptExecutor) {
	        ((JavascriptExecutor) driver)
	            .executeScript("javascript:window.scrollBy(250,350)");
	    }
	}
	
	public static void scrollToEnd(WebDriver driver) {
	    if (driver instanceof JavascriptExecutor) {
	        ((JavascriptExecutor) driver)
	            .executeScript("javascript:window.scrollBy("+driver.manage().window().getSize().getWidth()+","+driver.manage().window().getSize().getHeight()+")");
	    }
	}
	
	public static void swipeTo(AndroidDriver driver, int x, int y, int endx, int endy) {
	    driver.swipe(x, y, endx, endy, 200);
	}
}
