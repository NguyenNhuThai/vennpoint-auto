package com.hmvn.autotest.core;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StandardString {
	private static Date date = null;
	private static DateFormat dateFormat = null;
	private static String dateTime = "";
	
	/**
	 * 
	 * @param input
	 * @param replacedText
	 * @return
	 */
	public static String replaceWhiteSpace(String input, String replacedText) {
		String result = input.replaceAll("\\s+", replacedText);
		return result;
	}
	
	/**
	 * 
	 * @param format
	 * @param milis
	 * @return
	 */
	public static String convertMilisToDateTime(String format, long milis) {
		try {
			date = new Date(milis);
			dateFormat = new SimpleDateFormat(format);
			dateTime = dateFormat.format(date);
		} catch (Exception e) {
			e.printStackTrace();
			dateTime = "";
		}
		
		return dateTime;
	}
}
