package com.hmvn.autotest.core;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.testng.ITestResult;

import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;
import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.constants.TestCaseDetails;
import br.eti.kinoshita.testlinkjavaapi.model.Build;
import br.eti.kinoshita.testlinkjavaapi.model.Execution;
import br.eti.kinoshita.testlinkjavaapi.model.Platform;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.model.TestPlan;
import br.eti.kinoshita.testlinkjavaapi.model.TestSuite;

public class TMTAction {

	private TestLinkUtil util = new TestLinkUtil();

	public void createNewBuild() throws TestLinkAPIException {
		TestLinkAPIClient testlinkAPIClient = new TestLinkAPIClient(
				Constant.API_KEY_TESTLINK, Constant.SERVER_URL);
		String buildName = "Daily Test : " + new Date();
		String buildNotes = "Automation testing website adayroi.";
		if (Constant.BUILD_NAME.equalsIgnoreCase("not set")) {
			testlinkAPIClient.createBuild(Constant.PROJECT_NAME,
					Constant.PLAN_NAME, buildName, buildNotes);
			Constant.BUILD_NAME = buildName;
		}

	}

	public boolean updateResultToTestLink(ITestResult result,
			ExecutionStatus ex, String platformName, String notes) {
		try {
			TestLinkAPI api = new TestLinkAPI(new URL(Constant.SERVER_URL),
					Constant.API_KEY_TESTLINK);
			TestPlan testPlan = util.getTestPlanIDByName(Constant.PROJECT_NAME,
					Constant.PLAN_NAME);
			Platform platform = util.getPlatform(platformName, testPlan.getId());
			Build build = util.getBuildByName(Constant.BUILD_NAME,
					testPlan.getId());
			Integer testCaseId = util.getIdTestCase(result.getName());
			Integer testCaseExternalId = util.getExIdTestCase(result.getName());
			if (testPlan != null && platform != null && build != null
					&& testCaseId != null && testCaseExternalId != null) {
				api.reportTCResult(testCaseId, testCaseExternalId,
						testPlan.getId(), ex, build.getId(), build.getName(),
						notes, false, "", platform.getId(), platform.getName(),
						null, false);
				System.out.println("update result to test link success");
				System.out.println("----------------------------------");
				return true;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean updateResultToTestLinkWithAttactment(ITestResult result,
			ExecutionStatus ex, String platformName, String notes, File filePath) {
		try {
			TestLinkAPI api = new TestLinkAPI(new URL(Constant.SERVER_URL),
					Constant.API_KEY_TESTLINK);
			TestPlan testPlan = util.getTestPlanIDByName(Constant.PROJECT_NAME,
					Constant.PLAN_NAME);
			Platform platform = util.getPlatform(platformName, testPlan.getId());
			Build build = util.getBuildByName(Constant.BUILD_NAME,
					testPlan.getId());
			Integer testCaseId = util.getIdTestCase(result.getName());
			Integer testCaseExternalId = util.getExIdTestCase(result.getName());

			if (testPlan != null && platform != null && build != null
					&& testCaseId != null && testCaseExternalId != null) {
				api.reportTCResult(testCaseId, testCaseExternalId,
						testPlan.getId(), ex, build.getId(), build.getName(),
						notes, false, "", platform.getId(), platform.getName(),
						null, false);
				Execution e = api.getLastExecutionResult(testPlan.getId(),
						testCaseId, testCaseExternalId);
				String fileContent = null;
				byte[] byteArray = FileUtils.readFileToByteArray(filePath);
				fileContent = new String(Base64.encodeBase64(byteArray));
				api.uploadExecutionAttachment(e.getId(), "Screenshot", notes,
						filePath.getName(), "image/png", fileContent);
				System.out
						.println("update result and attactment to test link success");
				System.out.println("----------------------------------");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public ArrayList<String> getPreconditionsOfTestCase(String testcaseName) {
		ArrayList<String> pre = new ArrayList<String>();
		try {
			TestLinkAPI api = new TestLinkAPI(new URL(Constant.SERVER_URL),
					Constant.API_KEY_TESTLINK);
			TestPlan testPlan = util.getTestPlanIDByName(Constant.PROJECT_NAME,
					Constant.PLAN_NAME);
			TestSuite[] suites = api.getTestSuitesForTestPlan(testPlan.getId());
			for (TestSuite suite : suites) {
				TestCaseDetails details = TestCaseDetails.valueOf("FULL");
				TestCase[] cases = api.getTestCasesForTestSuite(suite.getId(),
						true, details);
				for (TestCase tcase : cases) {
					String name = tcase.getName().trim();
					if (name.equalsIgnoreCase(testcaseName)) {
						String temp = tcase.getPreconditions();
						Document doc = Jsoup.parse(temp);
						Elements els = doc.getElementsByTag("p");
						for (Element el : els) {
							System.out.println("precondion : " + el.text());
							pre.add(el.text());
						}
						break;
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return pre;
	}

	public ArrayList<String> getPreconditionsOfTestCase(String testcaseName,
			String testSuiteName) {
		ArrayList<String> pre = new ArrayList<String>();
		try {
			TestLinkAPI api = new TestLinkAPI(new URL(Constant.SERVER_URL),
					Constant.API_KEY_TESTLINK);
			TestPlan testPlan = util.getTestPlanIDByName(Constant.PROJECT_NAME,
					Constant.PLAN_NAME);
			TestSuite[] suites = api.getTestSuitesForTestPlan(testPlan.getId());
			for (TestSuite suite : suites) {
				if (suite.getName().equalsIgnoreCase(testSuiteName)) {
					TestCaseDetails details = TestCaseDetails.valueOf("FULL");
					TestCase[] cases = api.getTestCasesForTestSuite(
							suite.getId(), true, details);
					for (TestCase tcase : cases) {
						String name = tcase.getName().trim();
						if (name.equalsIgnoreCase(testcaseName)) {
							String temp = tcase.getPreconditions();
							Document doc = Jsoup.parse(temp);
							Elements els = doc.getElementsByTag("p");
							for (Element el : els) {
								System.out.println("precondion : " + el.text());
								pre.add(el.text());
							}
							break;
						}
					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return pre;
	}

}
