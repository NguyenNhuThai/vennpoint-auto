package com.hmvn.autotest.core;

import org.openqa.selenium.Dimension;



public class UserAgent {
	
	//IOS
	//320x480
	public static String IPhone4 = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5";
	public static Dimension Iphone4_Dimension = new Dimension(320,480);
	
	//320x568
	public static String Iphone5 = "Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X; en-us) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53";
	public static Dimension Iphone5_Dimension = new Dimension(320,568);
	//375x667
	public static String Iphone6 = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4";
	public static Dimension Iphone6_Dimension = new Dimension(375,667);
	
	//414x736
	public static String Iphone6Plus = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4";
	public static Dimension Iphone6Plus_Dimension = new Dimension(414,736);
	
	//Android
	//384x640
	public static String Nexus4 = "Mozilla/5.0 (Linux; Android 4.4.4; en-us; Nexus 4 Build/JOP40D) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2307.2 Mobile Safari/537.36";
	//360x640
	public static String Nexus5 = "Mozilla/5.0 (Linux; Android 4.4.4; en-us; Nexus 5 Build/JOP40D) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2307.2 Mobile Safari/537.36";
	//960x600
	public static String Nexus7 = "Mozilla/5.0 (Linux; Android 4.3; Nexus 7 Build/JSS15Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2307.2 Mobile Safari/537.36";
	public static Dimension Nexus7_Dimension = new Dimension(960,600);
	//360x640
	public static String GalaxyS4 = "Mozilla/5.0 (Linux; Android 4.4.2; GT-I9505 Build/JDQ39) AppleWebKit/537.36 (KHTML, like Gecko) Version/1.5 Chrome/28.0.1500.94 Mobile Safari/537.36";
	public static Dimension GalaxyS4_Dimension = new Dimension(360,640);
	
	//Blackberry BB10
	public static String BlackBerryZ30 = "Mozilla/5.0 (BB10; Touch) AppleWebKit/537.10+ (KHTML, like Gecko) Version/10.0.9.2372 Mobile Safari/537.10+";
	
	//Window phone 8.0
	//320x533 
	public static String Lumia520 = "Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; IEMobile/10.0; ARM; Touch; NOKIA; Lumia 520)";
	public static Dimension Lumia520_Dimension = new Dimension(320,533);
	
	
	
}
