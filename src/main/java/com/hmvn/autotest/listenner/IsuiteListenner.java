package com.hmvn.autotest.listenner;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.ISuite;
import org.testng.ISuiteListener;

import com.hmvn.autotest.core.APIUtil;
import com.hmvn.autotest.core.Constant;
import com.hmvn.autotest.core.FileManagerUtil;

public class IsuiteListenner implements ISuiteListener {

	public void onStart(ISuite suite) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmm");
		Date currentDate = new Date();
		System.out.println("Onstart suite: " +suite.getName());
		System.out.println("size method of suite" + suite.getAllMethods().size());
		try {
			InetAddress IP=InetAddress.getLocalHost();
			APIUtil.regisNewBuildTime(suite.getName(), dateFormat.format(currentDate), IP.getHostAddress());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (!suite.getAllMethods().isEmpty()) {
			FileManagerUtil.makeReportDirectory(suite.getName());
		} 
		Constant.currentRuntime = APIUtil.getRuntime(suite.getName());
		System.out.println("Current runtime: " +Constant.currentRuntime);
	}

	public void onFinish(ISuite suite) {
		System.out.println("Onfinish suite: " +suite.getName());
		if (!suite.getAllMethods().isEmpty()) {
			FileManagerUtil.copyReportToServer(suite.getName());
		}
	}

}
