package com.hmvn.autotest.listenner;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.hmvn.autotest.core.APIUtil;
import com.hmvn.autotest.core.FileManagerUtil;
import com.hmvn.autotest.core.ScreenRecord;


public class ItestListen implements ITestListener {

	boolean recordRunning = false;

	ScreenRecord record;
	private String fileName;

	public void onTestSuccess(ITestResult result) {
		System.out.println("Pass");
		APIUtil.pushPassTestToServer(result, FileManagerUtil.takeSnapshot(result.getName()));
	}

	public void onTestFailure(ITestResult result) {
		System.out.println("Fail");
		try {
			APIUtil.pushFailTestToServer(result, FileManagerUtil.takeSnapshot(result.getName()));
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void onTestSkipped(ITestResult result) {
		
	}

	public void onFinish(ITestContext context) {
	}

	public void onTestStart(ITestResult result) {
		System.out.println("================================================================================");
		System.out.println("===============================" + result.getName() + "=================================");
		System.out.println("================================================================================");
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

	}

	public void onStart(ITestContext context) {
		System.out.println("---------------------------" + context.getName() + "-------------------------------------");
	}

}
