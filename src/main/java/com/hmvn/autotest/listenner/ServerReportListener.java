package com.hmvn.autotest.listenner;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;

import org.apache.commons.io.FileUtils;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.xml.XmlSuite;

import com.hmvn.autotest.core.*;

public class ServerReportListener implements IReporter {
	private String formatDateTimeTestCaseTime = "dd-MM-yyyy HH:mm:ss";

	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

			String projectDirectory = PropertiesHelper.getKey("project.basedir");
			File htmlTemplateFile = new File(projectDirectory + File.separator + Constant.FOLDER_TEMPLATE_REPORT + File.separator + "reportBoostrapTemplate.html");
			String htmlString = FileUtils.readFileToString(htmlTemplateFile, "UTF-8");

			for (ISuite suite : suites) {
				Map<String, ISuiteResult> suiteResultMap = suite.getResults();
				for (ISuiteResult suiteResult : suiteResultMap.values()) {
					ITestContext testContext = suiteResult.getTestContext();

					Set<ITestResult> testResultPasses = testContext.getPassedTests().getAllResults();
					Set<ITestResult> testResultFails = testContext.getFailedTests().getAllResults();
					Set<ITestResult> testResultSkips = testContext.getSkippedTests().getAllResults();

					int numberPass = testResultPasses.size();
					int numberFail = testResultFails.size();
					int numberSkip = testResultSkips.size();
					int numberTestcase = numberPass + numberFail + numberSkip;
					String pass = getPassHtml(testContext);
					String fail = getFailHtml(testContext);
					String skip = getSkipHtml(testContext);

					String html = htmlString.replace("$suiteName", suite.getName()).replace("$date", dateFormat.format(testContext.getEndDate())).replace("$startTime", timeFormat.format(testContext.getStartDate()))
							.replace("$endTime", timeFormat.format(testContext.getEndDate())).replace("$numberTestcase", String.valueOf(numberTestcase)).replace("$numberPass", String.valueOf(numberPass))
							.replace("$numberFail", String.valueOf(numberFail)).replace("$numberSkip", String.valueOf(numberSkip)).replace("$pass", pass).replace("$fail", fail).replace("$skip", skip);

					FileManagerUtil.exportHtmlReport(html, suite);
					FileManagerUtil.openReportFile();
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private String getPassHtml(ITestContext testContext) throws IOException {
		SimpleDateFormat timeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		String projectDirectory = PropertiesHelper.getKey("project.basedir");
		File templateFile = new File(projectDirectory + File.separator + Constant.FOLDER_TEMPLATE_REPORT + File.separator + "passTemplate.html");
		String htmlTemplate = FileUtils.readFileToString(templateFile, "UTF-8");

		String html = "";
		int index = 1;
		for (ITestResult iTestResult : testContext.getPassedTests().getAllResults()) {
			List<String> logList = Reporter.getOutput(iTestResult);
			String log = "";
			for (int i = 0; i < logList.size(); i++) {
				log += logList.get(i);
			}

			long timeRun = iTestResult.getEndMillis() - iTestResult.getStartMillis();
			String time = "%d min %d sec";
			time = String.format(time, TimeUnit.MILLISECONDS.toMinutes(timeRun), TimeUnit.MILLISECONDS.toSeconds(timeRun) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeRun)));
			String testcaseName = iTestResult.getName();
			String imagePath = FileManagerUtil.getSnapshotFilePath(testcaseName);
			imagePath = imagePath.replaceAll(Matcher.quoteReplacement(File.separator), "/");

			String item = htmlTemplate.replace("$headingPass", "headingPass" + index).replace("$collapsePass", "collapsePass" + index).replace("$testcaseName", testcaseName).replace("$elapsedTime", time).replace("$log", log)
					.replace("$image", imagePath);

			html += item;
			index++;

			String testName = iTestResult.getName();
			String suiteName = testContext.getSuite().getName();
			System.out.println(testName);

			System.out.println("start time " + timeFormat.format(testContext.getStartDate()) + ", end time " + timeFormat.format(testContext.getEndDate()));
			// call api util
		}
		return html;
	}

	private String getFailHtml(ITestContext testContext) throws IOException {
		SimpleDateFormat timeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		String projectDirectory = PropertiesHelper.getKey("project.basedir");
		File templateFile = new File(projectDirectory + File.separator + Constant.FOLDER_TEMPLATE_REPORT + File.separator + "failTemplate.html");
		String htmlTemplate = FileUtils.readFileToString(templateFile, "UTF-8");

		String html = "";
		int index = 1;
		for (ITestResult iTestResult : testContext.getFailedTests().getAllResults()) {
			List<String> logList = Reporter.getOutput(iTestResult);
			String log = "";
			String exception = "";
			for (int i = 0; i < logList.size(); i++) {
				String logItem = logList.get(i);
				if (logItem.indexOf("[log-exception]") == 0) {
					exception = logItem.replace("[log-exception]", "");
				} else {
					log += logList.get(i);
				}
			}

			long timeRun = iTestResult.getEndMillis() - iTestResult.getStartMillis();
			String time = "%d min %d sec";
			time = String.format(time, TimeUnit.MILLISECONDS.toMinutes(timeRun), TimeUnit.MILLISECONDS.toSeconds(timeRun) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeRun)));
			String testcaseName = iTestResult.getName();
			String message = iTestResult.getThrowable().getMessage();
			if(message != null && message.contains("expected")){
				message = message.substring(0, message.indexOf("expected"));
			}
			
			String imagePath = FileManagerUtil.getSnapshotFilePath(testcaseName);
			imagePath = imagePath.replaceAll(Matcher.quoteReplacement(File.separator), "/");
			// should be remove the code block below
			if (exception.isEmpty() && iTestResult.getThrowable() != null) {
			//	exception = BoostrapReporter.getDetailException(iTestResult.getThrowable());
			}
			
			String item = htmlTemplate.replace("$headingFail", "headingFail" + index).replace("$collapseFail", "collapseFail" + index).replace("$testcaseName", testcaseName).replace("$elapsedTime", time).replace("$log", log)
					.replace("$image", imagePath).replace("$message", message).replace("$exceptionModal", "exceptionModal" + index).replace("$modalLabel", "modalLabel" + index).replace("$exception", exception);

			html += item;
			index++;

			String testName = iTestResult.getName();
			String suiteName = testContext.getSuite().getName();
			System.out.println(testName);
		

			System.out.println("start time " + timeFormat.format(testContext.getStartDate()) + ", end time " + timeFormat.format(testContext.getEndDate()));
			// call api util
		}
		return html;
	}

	private String getSkipHtml(ITestContext testContext) throws IOException {
		SimpleDateFormat timeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		String projectDirectory = PropertiesHelper.getKey("project.basedir");
		File templateFile = new File(projectDirectory + File.separator + Constant.FOLDER_TEMPLATE_REPORT + File.separator + "skipTemplate.html");
		String htmlTemplate = FileUtils.readFileToString(templateFile, "UTF-8");

		String html = "";
		int index = 1;
		for (ITestResult iTestResult : testContext.getSkippedTests().getAllResults()) {
			List<String> logList = Reporter.getOutput(iTestResult);
			String log = "";
			for (int i = 0; i < logList.size(); i++) {
				log += logList.get(i);
			}

			long timeRun = iTestResult.getEndMillis() - iTestResult.getStartMillis();
			String time = "%d min %d sec";
			time = String.format(time, TimeUnit.MILLISECONDS.toMinutes(timeRun), TimeUnit.MILLISECONDS.toSeconds(timeRun) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeRun)));
			String testcaseName = iTestResult.getName();
			String imagePath = FileManagerUtil.getSnapshotFilePath(testcaseName);
			imagePath = imagePath.replaceAll(Matcher.quoteReplacement(File.separator), "/");

			String item = htmlTemplate.replace("$headingSkip", "headingSkip" + index).replace("$collapseSkip", "collapseSkip" + index).replace("$testcaseName", testcaseName).replace("$elapsedTime", time).replace("$log", log)
					.replace("$image", imagePath);

			html += item;
			index++;

			String testName = iTestResult.getName();
			String suiteName = testContext.getSuite().getName();
			System.out.println(testName);
			//call api util
		}
		return html;
	}

}
