package com.hmvn.autotest.listenner;

import org.testng.ISuite;
import org.testng.ISuiteListener;

import com.hmvn.autotest.core.FileManagerUtil;

public class ServerSuiteListener implements ISuiteListener {

	public void onStart(ISuite suite) {
		System.out.println("size method of suite" + suite.getAllMethods().size());
		if (!suite.getAllMethods().isEmpty()) {
			FileManagerUtil.makeReportDirectory(suite.getName());
		} 
	}

	public void onFinish(ISuite suite) {
		// FileManagerUtil.zipReportDirectory();
		if (!suite.getAllMethods().isEmpty()) {
			FileManagerUtil.copyReportToServer(suite.getName());
		}
	}
}
