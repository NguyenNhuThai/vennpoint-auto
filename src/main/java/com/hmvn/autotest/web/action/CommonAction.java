package com.hmvn.autotest.web.action;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

import com.hmvn.autotest.core.Custom;
import com.hmvn.autotest.web.page.CommonPage;

public class CommonAction {
	CommonPage page = new CommonPage();
	
	public void click(WebDriver driver, final By locator) {
		Custom.waitForElementToBeClickable(driver,locator);
		driver.findElement(locator).click();
    }
	
    public boolean waitForLoadingImage(WebDriver driver){
    	Custom.checkElementUnExist(driver, CommonPage.LOADING_IMAGE);
    	return true;
    }
}
