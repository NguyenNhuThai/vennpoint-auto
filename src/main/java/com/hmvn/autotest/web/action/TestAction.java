package com.hmvn.autotest.web.action;

import org.openqa.selenium.WebDriver;

import com.hmvn.autotest.core.Constant;
import com.hmvn.autotest.core.Custom;
import com.hmvn.autotest.web.page.TestPage;

public class TestAction extends CommonAction{
	TestPage testpage = new TestPage();
	
	public boolean fillSearchData(WebDriver driver, String inputSearch){
		Custom.waitElementDisplay(driver, TestPage.INPUT_SEARCH, Constant.LONG_WAITING_TIME);
		testpage.inputSearch(driver).sendKeys(inputSearch);
		return driver.getCurrentUrl().contains(inputSearch);
	}
	
	public boolean goToReportPage(WebDriver driver){
		waitForLoadingImage(driver);
		Custom.waitDisplayedElement(testpage.reportLink(driver));
		Custom.clickElement(testpage.reportLink(driver));
		return Custom.waitForElementToBeVisibleOrNot(driver, TestPage.PASS_TAB);	
	}
	
	public boolean checkPassFailData(WebDriver driver, String passNum, String failNum){
		Boolean result = true;
		waitForLoadingImage(driver);
		if (!testpage.getPassTab(driver).getText().contains(passNum)) result = false;
		if (!testpage.getFailTab(driver).getText().contains(failNum)) result = false;
		return result;
	}
}
