package com.hmvn.autotest.web.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CommonPage {
    public static final By LOADING_IMAGE = By.xpath("//img[@src='/img/logo.png']");
    
	public WebElement inputSearch(WebDriver driver){
		WebElement el = driver.findElement(LOADING_IMAGE);
		return el;
	}
}
