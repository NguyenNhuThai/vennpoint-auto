package com.hmvn.autotest.web.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TestPage extends CommonPage{
    public static final By INPUT_SEARCH = By.id("lst-ib");
    public static final By REPORT_LINK = By.xpath("//div[@class='well']//a[@href='report.html']");
    public static final By PASS_TAB = By.id("pass-tab");
    public static final By FAIL_TAB = By.id("fail-tab");
    
	public WebElement inputSearch(WebDriver driver){
		WebElement el = driver.findElement(INPUT_SEARCH);
		return el;
	}
	
	public WebElement reportLink(WebDriver driver){
		WebElement el = driver.findElement(REPORT_LINK);
		return el;
	}
	
	public WebElement getPassTab(WebDriver driver){
		WebElement el = driver.findElement(PASS_TAB);
		return el;
	}
	
	public WebElement getFailTab(WebDriver driver){
		WebElement el = driver.findElement(FAIL_TAB);
		return el;
	}	
	
}
