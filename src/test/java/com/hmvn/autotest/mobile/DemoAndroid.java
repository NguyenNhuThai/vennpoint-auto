package com.hmvn.autotest.mobile;

import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.hmvn.autotest.core.AppiumUtil;
import com.hmvn.autotest.core.Json;
import com.hmvn.autotest.core.TakeScreenShot;
import com.hmvn.autotest.web.action.TestAction;

import io.appium.java_client.android.AndroidDriver;

public class DemoAndroid {
	private AndroidDriver driver;
	private	JsonObject testData;
	private TestAction action = new TestAction();

	@Parameters({"platformName", "platformVer", "deviceName", "appName", "appPackage","data"})
	@BeforeSuite
	public void beforeSuite(String platformName, String platformVer, String deviceName, String appName, String appPackage, String data) {
		AppiumUtil.startServer("windows");
		testData = Json.getJsonData(data);
		driver = AppiumUtil.getDriverInstance(platformName, platformVer, deviceName, appName, appPackage);
		TakeScreenShot.init(driver);
		Set<String> contextNames = driver.getContextHandles();
		for (String contextName : contextNames) {
			if(contextName.equals("WEBVIEW_thai.phonegap")){
				driver.context(contextName);
			}
		}
	}
	
	  @Test
	  public void demoTestCase() {
		  Assert.assertTrue(action.goToReportPage(driver),"Can't go to report page");
	  }
	  
	  @Test
	  public void getPassFailNumber(){
		  Assert.assertTrue(action.checkPassFailData(driver, testData.get("pass_num").getAsString(), testData.get("fail_num").getAsString()),"Data is not corrected");
	  }
	  
	  @AfterSuite(alwaysRun = true)
	  public void afterSuite(){
		  AppiumUtil.stopServer("windows");
	  }
}
