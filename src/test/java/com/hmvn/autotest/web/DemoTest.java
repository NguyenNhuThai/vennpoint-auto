package com.hmvn.autotest.web;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.hmvn.autotest.core.AppiumUtil;
import com.hmvn.autotest.core.DriverUtil;
import com.hmvn.autotest.core.Json;
import com.hmvn.autotest.core.TakeScreenShot;
import com.hmvn.autotest.web.action.TestAction;

public class DemoTest {
	private WebDriver driver;
	private	JsonObject testData;
	private TestAction action = new TestAction();
	@Parameters({"browser", "data"})
	@BeforeSuite
	public void beforeSuite(String browser, String data) {
		driver = DriverUtil.getInstance(browser);
		testData = Json.getJsonData(data);
		TakeScreenShot.init(driver);
	}
	
  @Test
  public void demoTestCase() {
	  driver.get(testData.get("link").getAsString());
	  Assert.assertTrue(action.goToReportPage(driver),"Can't go to report page");
  }
  
  @Test
  public void getPassFailNumber(){
	  Assert.assertTrue(action.checkPassFailData(driver, testData.get("pass_num").getAsString(), testData.get("fail_num").getAsString()),"Data is not corrected");
  }
  @AfterSuite(alwaysRun = true)
  public void afterSuite(){
	  driver.quit();
  }
}
