document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady(){
    console.log("OMG, It' RUNNING, OMG")

    $("#resole").html(getReport());
    getChart();
} 

function getReport(){
    var link = "http://10.0.0.99:2706/TodayReportMobile"
    var xhttp = new XMLHttpRequest();
	xhttp.open("GET",link , false);
	xhttp.setRequestHeader("Content-type", "application/json");
	xhttp.send();
	return xhttp.responseText;
}
function getChart(){
    var a = $("#pass-tab").text();
    var b = $("#fail-tab").text();
    console.log(a.substring(5,7))
    console.log(b.substring(5,7))
    var chartData = [{
					name: "Khối lượng",
					colorByPoint: true,
					data: [{
						name: "Pass",
						color: "#5cb85c",
						y:parseInt(a.substring(5,7))
					}, {
						name: "Fail",
						color: "#d9534f",
						y:parseInt(b.substring(5,7))
					}]
				}];

    
   var chart = $('#container1').highcharts({
				chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false,
					type: 'pie'
				},
				title: {
					text: 'Automation test report of HM portal at $date'
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						dataLabels: {
							enabled: false
						},
						showInLegend: true
					}
				},
				series: chartData
	});

}
